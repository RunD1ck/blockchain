package ru.tikoro.blockchainapp.presentation.presenter

import kotlinx.coroutines.*
import moxy.MvpPresenter
import ru.tikoro.blockchainapp.presentation.view.BaseView
import kotlin.coroutines.CoroutineContext

abstract class BasePresenter<View : BaseView> : MvpPresenter<View>() {
    private val exHandler = CoroutineExceptionHandler { _, exception -> }
    private val job = SupervisorJob()
    private val coroutineContext: CoroutineContext = Dispatchers.IO + exHandler + job
    val coroutineScope = CoroutineScope(coroutineContext)

    override fun onDestroy() {
        coroutineContext.cancel()
        super.onDestroy()
    }
}