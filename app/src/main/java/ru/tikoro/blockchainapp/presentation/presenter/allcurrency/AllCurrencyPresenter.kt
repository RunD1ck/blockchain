package ru.tikoro.blockchainapp.presentation.presenter.allcurrency

import android.util.Log
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import ru.tikoro.blockchainapp.model.interactor.allcurrency.AllCurrencyInteractor
import ru.tikoro.blockchainapp.presentation.view.allcurrency.AllCurrencyView
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.tikoro.blockchainapp.presentation.presenter.BasePresenter
import ru.tikoro.blockchainapp.ui.pagination.CurrencyDataSource
import java.lang.Exception

@InjectViewState
class AllCurrencyPresenter : BasePresenter<AllCurrencyView>(), KoinComponent {
    private val interactor: AllCurrencyInteractor by inject()

    private val PAGED_SIZE = 10
    override fun onFirstViewAttach() {
        getAllCurrency()
    }


    private fun getAllCurrency() {
        val pagedLiveData = LivePagedListBuilder(
            CurrencyDataSource.Factory(
                coroutineScope,
                interactor
            ),
            PagedList
                .Config
                .Builder()
                .setPageSize(PAGED_SIZE)
                .build()

        ).build()
        pagedLiveData.observeForever {
            viewState.onShowCurrency(it)
        }
    }
}
