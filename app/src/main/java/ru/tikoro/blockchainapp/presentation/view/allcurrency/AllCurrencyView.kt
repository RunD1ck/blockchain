package ru.tikoro.blockchainapp.presentation.view.allcurrency

import androidx.paging.PagedList
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType
import ru.tikoro.blockchainapp.entity.allcurrency.CurrencyItem
import ru.tikoro.blockchainapp.entity.dto.allcurrency.CurrencyDTO
import ru.tikoro.blockchainapp.presentation.view.BaseView


interface AllCurrencyView : BaseView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onShowCurrency(list: PagedList<CurrencyItem>)
}