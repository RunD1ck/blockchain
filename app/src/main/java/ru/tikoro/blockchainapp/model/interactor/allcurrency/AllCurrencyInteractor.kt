package ru.tikoro.blockchainapp.model.interactor.allcurrency

import ru.tikoro.blockchainapp.entity.dto.allcurrency.toCurrencyItem
import ru.tikoro.blockchainapp.model.repository.CurrencyRepository

class AllCurrencyInteractor(private val currencyRepository: CurrencyRepository) {

    suspend fun getAllCurrency(offset: Int) =
        currencyRepository.getCoins(offset).data.coins.map {
            it.toCurrencyItem()
        }
}