package ru.tikoro.blockchainapp.model.server

import retrofit2.http.GET
import retrofit2.http.Query
import ru.tikoro.blockchainapp.entity.dto.BaseResponse
import ru.tikoro.blockchainapp.entity.dto.allcurrency.CurrencyDTO

interface BlockChainApi {

    @GET("coins")
    suspend fun getCoins(@Query("offset") offset: Int): BaseResponse<List<CurrencyDTO>>
}