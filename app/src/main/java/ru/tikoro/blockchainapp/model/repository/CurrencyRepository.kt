package ru.tikoro.blockchainapp.model.repository

import ru.tikoro.blockchainapp.model.server.BlockChainApi

class CurrencyRepository(private val api: BlockChainApi) {

    suspend fun getCoins(offset: Int) =
        api.getCoins(offset)
}