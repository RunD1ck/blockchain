package ru.tikoro.blockchainapp.entity.allcurrency

data class CurrencyItem(
    val id: Int,
    val image: String,
    val name: String,
    val symbol: String,
    val price: Float
)