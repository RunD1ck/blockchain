package ru.tikoro.blockchainapp.entity.dto.allcurrency

import com.squareup.moshi.Json
import ru.tikoro.blockchainapp.entity.allcurrency.CurrencyItem

data class CurrencyDTO(
    @field:Json(name = "id")
    val id: Int,
    @field:Json(name = "symbol")
    val symbol: String,
    @field:Json(name = "name")
    val name: String,
    @field:Json(name = "description")
    val description: String,
    @field:Json(name = "iconUrl")
    val image: String,
    @field:Json(name = "price")
    val price: Float
)

fun CurrencyDTO.toCurrencyItem() =
    CurrencyItem(this.id, this.image, this.name, this.symbol, this.price)