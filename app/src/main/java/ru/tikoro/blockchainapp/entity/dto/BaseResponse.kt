package ru.tikoro.blockchainapp.entity.dto

data class BaseResponse<T>(
    val status: String,
    val data: DataResponse<T>
)


data class DataResponse<T>(
    val coins: T
)