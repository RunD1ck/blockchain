package ru.tikoro.blockchainapp.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.tikoro.blockchainapp.BuildConfig.BASE_URL
import ru.tikoro.blockchainapp.model.server.BlockChainApi
import java.util.concurrent.TimeUnit

val networkModule = module {
    factory { provideDefaultOkhttpClient(androidContext()) }
    single { provideBlockChainApiService(provideRetrofit(androidContext(), BASE_URL)) }
}

fun provideDefaultOkhttpClient(contex: Context): OkHttpClient.Builder {
    return OkHttpClient.Builder()
        .addInterceptor(ChuckerInterceptor(contex))
        .readTimeout(90, TimeUnit.SECONDS)
        .connectTimeout(90, TimeUnit.SECONDS)
}

fun provideRetrofit(contex: Context, baseUrl: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(provideDefaultOkhttpClient(contex).build())
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
}

fun provideBlockChainApiService(retrofit: Retrofit): BlockChainApi =
    retrofit.create(BlockChainApi::class.java)