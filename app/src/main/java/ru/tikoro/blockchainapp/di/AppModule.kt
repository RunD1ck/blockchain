package ru.tikoro.blockchainapp.di

import org.koin.dsl.module
import ru.tikoro.blockchainapp.model.interactor.allcurrency.AllCurrencyInteractor
import ru.tikoro.blockchainapp.model.repository.CurrencyRepository

val appModule = module {

    factory { AllCurrencyInteractor(get()) }
    factory { CurrencyRepository(get()) }
}