package ru.tikoro.blockchainapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ru.tikoro.blockchainapp.di.appModule
import ru.tikoro.blockchainapp.di.networkModule

class App : Application() {


    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    appModule, networkModule
                )
            )
        }

    }
}