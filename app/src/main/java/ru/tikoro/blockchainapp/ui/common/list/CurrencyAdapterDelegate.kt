package ru.tikoro.blockchainapp.ui.common.list

import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.module.AppGlideModule
import com.caverock.androidsvg.SVGImageView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import ru.tikoro.blockchainapp.R
import ru.tikoro.blockchainapp.entity.allcurrency.CurrencyItem
import ru.tikoro.blockchainapp.ui.util.GlideApp
import ru.tikoro.blockchainapp.ui.util.SvgSoftwareLayerSetter

fun currencyAdapterDelegate() =
    adapterDelegate<CurrencyItem, CurrencyItem>(R.layout.rv_currency_item) {

        val ivIcon: ImageView = findViewById(R.id.ivIcon)
        val tvName: TextView = findViewById(R.id.tvName)
        val tvSymbol: TextView = findViewById(R.id.tvSymbol)
        val tvPrice: TextView = findViewById(R.id.tvPrice)

        bind {
            GlideApp.with(context)
                .`as`(PictureDrawable::class.java)
                .listener(SvgSoftwareLayerSetter())
                .load(item.image)
                .into(ivIcon)

            tvName.text = item.name
            tvSymbol.text = item.symbol
            tvPrice.text = item.price.toString()

        }
    }