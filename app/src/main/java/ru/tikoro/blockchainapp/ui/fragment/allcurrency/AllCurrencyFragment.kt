package ru.tikoro.blockchainapp.ui.fragment.allcurrency

import android.os.Bundle
import android.view.View
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_all_currency.*
import moxy.ktx.moxyPresenter
import ru.tikoro.blockchainapp.R
import ru.tikoro.blockchainapp.entity.allcurrency.CurrencyItem
import ru.tikoro.blockchainapp.presentation.presenter.allcurrency.AllCurrencyPresenter
import ru.tikoro.blockchainapp.presentation.view.allcurrency.AllCurrencyView
import ru.tikoro.blockchainapp.ui.common.BaseFragment
import ru.tikoro.blockchainapp.ui.common.adapter.AllCurrencyRVAdapter


class AllCurrencyFragment : BaseFragment(),
    AllCurrencyView {

    override val layoutResId = R.layout.fragment_all_currency

    private val presenter by moxyPresenter { AllCurrencyPresenter() }

    private val adapter: AllCurrencyRVAdapter by lazy {
        AllCurrencyRVAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvCurrency.layoutManager = LinearLayoutManager(requireContext())
        rvCurrency.adapter = adapter

    }

    override fun onShowCurrency(list: PagedList<CurrencyItem>) {
        adapter.submitList(list)
    }
}