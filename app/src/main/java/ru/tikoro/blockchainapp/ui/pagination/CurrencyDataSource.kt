package ru.tikoro.blockchainapp.ui.pagination

import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.tikoro.blockchainapp.entity.allcurrency.CurrencyItem
import ru.tikoro.blockchainapp.entity.dto.allcurrency.CurrencyDTO
import ru.tikoro.blockchainapp.model.interactor.allcurrency.AllCurrencyInteractor

class CurrencyDataSource(
    private val coroutineScope: CoroutineScope,
    private val interactor: AllCurrencyInteractor
) : PageKeyedDataSource<Int, CurrencyItem>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, CurrencyItem>
    ) {
        coroutineScope.launch {
            val list = interactor.getAllCurrency(0)
            withContext(Dispatchers.Main) {
                callback.onResult(list, null, 1)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, CurrencyItem>) {
        coroutineScope.launch {
            val list = interactor.getAllCurrency(params.key)
            withContext(Dispatchers.Main) {
                callback.onResult(list, params.key + 1)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, CurrencyItem>) {
        coroutineScope.launch {
            val list = interactor.getAllCurrency(params.key)
            withContext(Dispatchers.Main) {
                callback.onResult(list, params.key - 1)
            }
        }
    }

    class Factory(
        private val coroutineScope: CoroutineScope,
        private val feedInteractor: AllCurrencyInteractor
    ) : DataSource.Factory<Int, CurrencyItem>() {
        override fun create(): DataSource<Int, CurrencyItem> {
            return CurrencyDataSource(coroutineScope, feedInteractor)
        }
    }

}