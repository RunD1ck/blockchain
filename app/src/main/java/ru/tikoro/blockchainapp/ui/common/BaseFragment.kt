package ru.tikoro.blockchainapp.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import moxy.MvpAppCompatFragment
import ru.tikoro.blockchainapp.presentation.view.BaseView

abstract class BaseFragment : MvpAppCompatFragment(), BaseView {

    abstract val layoutResId: Int?

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return layoutResId?.let { inflater.inflate(it, container, false) }
    }

}