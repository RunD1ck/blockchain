package ru.tikoro.blockchainapp.ui.common.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import com.hannesdorfmann.adapterdelegates4.paging.PagedListDelegationAdapter
import ru.tikoro.blockchainapp.entity.allcurrency.CurrencyItem
import ru.tikoro.blockchainapp.ui.common.list.currencyAdapterDelegate

fun CurrencyItem.isSame(newItem: CurrencyItem) =
    this.id == newItem.id && this.price == newItem.price

class AllCurrencyRVAdapter : PagedListDelegationAdapter<CurrencyItem>(
    AdapterDelegatesManager(
        currencyAdapterDelegate()
    ),
    object : DiffUtil.ItemCallback<CurrencyItem>() {
        override fun areItemsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
            return oldItem.isSame(newItem)
        }

        override fun areContentsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
            return oldItem.isSame(newItem)
        }
    }
)